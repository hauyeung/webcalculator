﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebCalculator
{
    public partial class _Default : System.Web.UI.Page
    {
        // Private class variables
        private string _currentValueString = "";
        private double _currentValueDouble = 0;
        private string _pendingAction = "";
        private string _pendingValueString = "";
        private double _pendingValueDouble = 0.0;
        private bool _firstOperationField = true;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Initialize our calculator window
                resultTextBox.Text = "0";
            }
            else
            {
                // Respond to postback
                // Grab calculator window, convert and save values as necessary
                _currentValueString = resultTextBox.Text;
                if (_currentValueString.Length > 0)
                    _currentValueDouble = double.Parse(_currentValueString);

                // Save and convert hidden field values
                _firstOperationField = bool.Parse(firstOperationField.Value);
                _pendingAction = pendingActionField.Value;
                _pendingValueString = pendingValueField.Value;
                if (_pendingValueString.Length > 0)
                    _pendingValueDouble = double.Parse(_pendingValueString);
            }

            // Add event delegates to number buttons
            zeroButton.Click += new EventHandler(numberButton_Click);
            oneButton.Click += new EventHandler(numberButton_Click);
            twoButton.Click += new EventHandler(numberButton_Click);
            threeButton.Click += new EventHandler(numberButton_Click);
            fourButton.Click += new EventHandler(numberButton_Click);
            fiveButton.Click += new EventHandler(numberButton_Click);
            sixButton.Click += new EventHandler(numberButton_Click);
            sevenButton.Click += new EventHandler(numberButton_Click);
            eightButton.Click += new EventHandler(numberButton_Click);
            nineButton.Click += new EventHandler(numberButton_Click);

            // Add event delegates for action buttons
            divideButton.Click += new EventHandler(actionButton_Click);
            multiplyButton.Click += new EventHandler(actionButton_Click);
            subtractButton.Click += new EventHandler(actionButton_Click);
            addButton.Click += new EventHandler(actionButton_Click);
            equalButton.Click += new EventHandler(actionButton_Click);
            clearErrorButton.Click += new EventHandler(actionButton_Click);
            clearButton.Click += new EventHandler(actionButton_Click);
            backspaceButton.Click += new EventHandler(actionButton_Click);
            signButton.Click += new EventHandler(actionButton_Click);
            pointButton.Click += new EventHandler(actionButton_Click);


        }
        void numberButton_Click(object sender, EventArgs e)
        {
            // Determine number by extracting number from Button Text property
            int number = Int32.Parse((sender as Button).Text);

            // Set/append number to calculator window
            if (_firstOperationField)
                resultTextBox.Text = number.ToString();
            else
            {
                // Prevent prepending a 0 to a number
                if (_currentValueString == "0") _currentValueString = "";
                resultTextBox.Text = (_currentValueString + number.ToString());
            }


            // Indicate no longer first operation
            firstOperationField.Value = "false";

        }

        void actionButton_Click(object sender, EventArgs e)
        {
            // Determine action
            string currentAction = (sender as Button).Text;

            // Determine if there is a pending operation
            bool pendingOperation = (_pendingValueString.Length > 0 &&
                _currentValueString.Length > 0 && _pendingAction.Length > 0);

            // Perform previous action
            if ("/*-+=".IndexOf(currentAction) >= 0)
            {
                // Handle arithmetic operation actions
                if (pendingOperation)
                    resultTextBox.Text = doCalculation(_pendingAction, _pendingValueDouble, _currentValueDouble).ToString();

                // Update first operation hidden field
                firstOperationField.Value = "true";

                // Update previous value and action hidden fields
                if (currentAction != "=")
                {
                    // Support chaining calculations
                    pendingActionField.Value = currentAction;
                    pendingValueField.Value = resultTextBox.Text;
                }
                else
                {
                    pendingActionField.Value = "";
                    pendingValueField.Value = "";
                }
            }
            else
            {
                // Handle remaining actions
                switch (currentAction)
                {
                    case "C":
                        // Reset calculator window and hidden fields
                        resultTextBox.Text = "0";
                        pendingActionField.Value = "";
                        pendingValueField.Value = "";
                        firstOperationField.Value = "true";
                        break;
                    case "CE":
                        // Clear error
                        resultTextBox.Text = "0";
                        firstOperationField.Value = "true";
                        break;
                    case "<-":
                        // Backspace - prevent leaving "bad" data in calculator window
                        string newResult = _currentValueString.Substring(0, _currentValueString.Length - 1);
                        if (newResult.Length == 0 || newResult == "-")
                            resultTextBox.Text = "0";
                        else
                            resultTextBox.Text = newResult;
                        break;
                    case ".":
                        // Decimal point
                        if (_currentValueString.IndexOf(".") < 0) resultTextBox.Text = _currentValueString + ".";
                        break;
                    case "+/-":
                        // Sign
                        resultTextBox.Text = (_currentValueDouble * -1).ToString();
                        break;
                }
            }



        }
        private double doCalculation(string action, double left, double right)
        {
            // Perform arithmetic calculations
            double result = 0.0;
            switch (action)
            {
                case "/":
                    // Prevent divide by zero
                    if (right != 0)
                        result = left / right;
                    else
                    {
                        // TODO: Handle divide by zero error
                        errorTextBox.Text = "Cannot divide by zero.";
                    }
                    break;
                case "*":
                    result = left * right;
                    break;
                case "-":
                    result = left - right;
                    break;
                case "+":
                    result = left + right;
                    break;
            }
            return result;
        }


    }
}
